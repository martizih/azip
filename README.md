# Asynchronous ZIP Operator

The `azip()` function takes both synchronous and asynchronous iterables (can be zero or more), awaits their results one-by-one and aggregates them into a tuple.
It stops if one of the iterators is exhausted. It waits for the next value of all iterators before yielding the tuple.

## Installation

```sh
python -m pip install azip
```

## Usage

```python
from azip.azip import azip

async for s, a in azip(sync_iter, async_iter):
  print(s, a)
```
