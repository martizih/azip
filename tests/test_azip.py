import pytest

from src.azip.azip import azip


@pytest.mark.asyncio
async def test_empty():
    assert [x async for x in azip()] == []


@pytest.mark.asyncio
async def test_list():
    assert [x async for x in azip([1, 2, 3, 4])] == [(1,), (2,), (3,), (4,)]


@pytest.mark.asyncio
async def test_lists():
    assert [x async for x in azip([1, 2, 3, 4], [5, 6, 7, 8])] == [(1, 5), (2, 6), (3, 7), (4, 8)]


@pytest.mark.asyncio
async def test_generator():
    def a():
        yield from [1, 2, 3, 4]

    assert [x async for x in azip(a())] == [(1,), (2,), (3,), (4,)]


@pytest.mark.asyncio
async def test_generators():
    def a():
        yield from [1, 2, 3, 4]

    def b():
        yield from [5, 6, 7, 8]

    assert [x async for x in azip(a(), b())] == [(1, 5), (2, 6), (3, 7), (4, 8)]


@pytest.mark.asyncio
async def test_async_generator():
    async def a():
        for x in [1, 2, 3, 4]:
            yield x

    assert [x async for x in azip(a())] == [(1,), (2,), (3,), (4,)]


@pytest.mark.asyncio
async def test_async_generators():
    async def a():
        for x in [1, 2, 3, 4]:
            yield x

    async def b():
        for x in [5, 6, 7, 8]:
            yield x

    assert [x async for x in azip(a(), b())] == [(1, 5), (2, 6), (3, 7), (4, 8)]


@pytest.mark.asyncio
async def test_mixed_generators():
    def a():
        yield from [1, 2, 3, 4]

    async def b():
        for x in [5, 6, 7, 8]:
            yield x

    assert [x async for x in azip(a(), b())] == [(1, 5), (2, 6), (3, 7), (4, 8)]
